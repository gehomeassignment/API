package com.codecool.ge.tests;


import com.codecool.ge.apis.BookingApi;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.HashMap;
import java.util.Map;

public class BookingTest {
    BookingApi bookingApi = new BookingApi();
    int bookingId;


    @Test(description = "Create new booking entry and verify details",
            priority = 1)
    public void createNewBooking() {
        Map<String, Object> newBooking = bookingApi.createNewBooking();
        HashMap<String, String> bookingDates = (HashMap<String, String>) newBooking.get("bookingdates");
        bookingId = bookingApi.getBookingId();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(newBooking.get("firstname"), BookingApi.FIRSTNAME);
        softAssert.assertEquals(newBooking.get("lastname"), BookingApi.LASTNAME);
        softAssert.assertEquals(newBooking.get("totalprice"), BookingApi.TOTAL_PRICE);
        softAssert.assertEquals(newBooking.get("depositpaid"), BookingApi.DEPOSIT_PAID);
        softAssert.assertEquals(bookingDates.get("checkin"), BookingApi.CHECKIN);
        softAssert.assertEquals(bookingDates.get("checkout"), BookingApi.CHECKOUT);
        softAssert.assertEquals(newBooking.get("additionalneeds"), BookingApi.ADDITIONAL);
        softAssert.assertAll();
    }

    @Test(description = "Retrieve newly created booking and verify details",
            dependsOnMethods = {"createNewBooking"},
            priority = 2)
    public void retrieveBookingById() {
        Map<String, Object> bookingById = bookingApi.retrieveBookingById(bookingId);
        HashMap<String, String> bookingDates = (HashMap<String, String>) bookingById.get("bookingdates");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(bookingById.get("firstname"), BookingApi.FIRSTNAME);
        softAssert.assertEquals(bookingById.get("lastname"), BookingApi.LASTNAME);
        softAssert.assertEquals(bookingById.get("totalprice"), BookingApi.TOTAL_PRICE);
        softAssert.assertEquals(bookingById.get("depositpaid"), BookingApi.DEPOSIT_PAID);
        softAssert.assertEquals(bookingDates.get("checkin"), BookingApi.CHECKIN);
        softAssert.assertEquals(bookingDates.get("checkout"), BookingApi.CHECKOUT);
        softAssert.assertEquals(bookingById.get("additionalneeds"), BookingApi.ADDITIONAL);
        softAssert.assertAll();
    }

    @Test(description = "Update booking and verify details",
            dependsOnMethods = {"createNewBooking"},
            priority = 3)
    public void updateBookingById() {
        Map<String, Object> updatedBooking = bookingApi.updateBookingById(bookingId);
        HashMap<String, String> bookingDates = (HashMap<String, String>) updatedBooking.get("bookingdates");

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(updatedBooking.get("firstname"), BookingApi.UPDATED_FIRSTNAME);
        softAssert.assertEquals(updatedBooking.get("lastname"), BookingApi.LASTNAME);
        softAssert.assertEquals(updatedBooking.get("totalprice"), BookingApi.TOTAL_PRICE);
        softAssert.assertEquals(updatedBooking.get("depositpaid"), BookingApi.DEPOSIT_PAID);
        softAssert.assertEquals(bookingDates.get("checkin"), BookingApi.CHECKIN);
        softAssert.assertEquals(bookingDates.get("checkout"), BookingApi.CHECKOUT);
        softAssert.assertEquals(updatedBooking.get("additionalneeds"), BookingApi.ADDITIONAL);
        softAssert.assertAll();
    }

    @Test(description = "Delete booking and verify deletion",
            dependsOnMethods = {"createNewBooking"},
            priority = 4)
    public void deleteBookingById() {
        int actualStatusCode = bookingApi.deleteBookingById(bookingId);
        Assert.assertEquals(actualStatusCode, 201);
        Assert.assertNull(bookingApi.retrieveBookingById(bookingId));
    }

}
