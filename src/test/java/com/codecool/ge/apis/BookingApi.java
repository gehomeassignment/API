package com.codecool.ge.apis;

import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.get;
import static io.restassured.RestAssured.given;

public class BookingApi {
    private AuthApi authApi = new AuthApi();

    private String bookingEndpoint = "https://restful-booker.herokuapp.com/booking/";
    public static final String FIRSTNAME = "Katalin";
    public static final String LASTNAME = "Gabor";
    public static final int TOTAL_PRICE = 111;
    public static final boolean DEPOSIT_PAID = true;
    public static final String CHECKIN = "2023-08-14";
    public static final String CHECKOUT = "2023-08-20";
    public static final String ADDITIONAL = "Breakfast";
    public static final String UPDATED_FIRSTNAME = "Katalin Petra";
    private final String[] fields =
            new String[]{"firstname", "lastname", "totalprice", "depositpaid", "bookingdates", "additionalneeds"};
    private String token;


    private JSONObject createNewBookingLoad() {
        JSONObject bookingDates = new JSONObject();
        bookingDates.put("checkin", CHECKIN);
        bookingDates.put("checkout", CHECKOUT);

        JSONObject newBookingLoad = new JSONObject();
        newBookingLoad.put("firstname", FIRSTNAME);
        newBookingLoad.put("lastname", LASTNAME);
        newBookingLoad.put("totalprice", TOTAL_PRICE);
        newBookingLoad.put("depositpaid", DEPOSIT_PAID);
        newBookingLoad.put("bookingdates", bookingDates);
        newBookingLoad.put("additionalneeds", ADDITIONAL);
        return newBookingLoad;
    }

    private JSONObject updateBookingLoad() {
        JSONObject bookingDates = new JSONObject();
        bookingDates.put("checkin", CHECKIN);
        bookingDates.put("checkout", CHECKOUT);

        JSONObject updatedBookingLoad = new JSONObject();
        updatedBookingLoad.put("firstname", UPDATED_FIRSTNAME);
        updatedBookingLoad.put("lastname", LASTNAME);
        updatedBookingLoad.put("totalprice", TOTAL_PRICE);
        updatedBookingLoad.put("depositpaid", DEPOSIT_PAID);
        updatedBookingLoad.put("bookingdates", bookingDates);
        updatedBookingLoad.put("additionalneeds", ADDITIONAL);
        return updatedBookingLoad;
    }

    private Response postNewBooking() {
        String load = createNewBookingLoad().toString();
        Response newBookingresponse =
                given()
                        .contentType(ContentType.JSON)
                        .body(load)
                        .post(bookingEndpoint);
        return newBookingresponse;
    }

    private Response getBookingById(int id) {
        Response bookingByIdResponse = get(bookingEndpoint + id);
        return bookingByIdResponse;
    }

    private Response putBookingById(int id, String authToken) {
        String load = updateBookingLoad().toString();
        Response updatedBookingByIdResponse =
                given()
                        .contentType(ContentType.JSON)
                        .accept("application/json")
                        .header("Cookie", "token=" + authToken)
                        .body(load)
                        .put(bookingEndpoint + id);
        return updatedBookingByIdResponse;
    }

    private Response deleteBookingById(int id, String authToken) {
        Response deleteBookingResponse =
                given()
                        .contentType(ContentType.JSON)
                        .header("Cookie", "token=" + authToken)
                        .delete(bookingEndpoint + id);
        return deleteBookingResponse;
    }

    private void printResponse(Response response, String text) {
        System.out.println(text);
        response.prettyPrint();
    }

    private boolean isStatusOk(Response response) {
        int statusCode = response.getStatusCode();
        return statusCode == 200;
    }

    private Map<String, Object> getResponseJsonElements(Response response, String key) {
        Map<String, Object> jsonElements = response.jsonPath().get(key);
        return jsonElements;
    }

    private Map<String, Object> getResponseJsonElements(Response response, String[] keys) {
        Map<String, Object> jsonElements = new HashMap<>();
        for (String key : keys) {
            jsonElements.put(key, response.jsonPath().get(key));
        }
        return jsonElements;
    }

    public int getBookingId() {
        return postNewBooking().jsonPath().get("bookingid");
    }

    private void getToken() {
        token = authApi.getToken();
    }

    public Map<String, Object> createNewBooking() {
        Response newBookingResponse = postNewBooking();
        if (isStatusOk(newBookingResponse)) {
            printResponse(newBookingResponse, "\nNew booking response:");
            return getResponseJsonElements(newBookingResponse, "booking");
        }
        return null;
    }

    public Map<String, Object> retrieveBookingById(int id) {
        Response bookingByIdResponse = getBookingById(id);
        if (isStatusOk(bookingByIdResponse)) {
            printResponse(bookingByIdResponse, "\nRetrieve booking by ID response:");
            return getResponseJsonElements(bookingByIdResponse, fields);
        }
        return null;
    }

    public Map<String, Object> updateBookingById(int id) {
        getToken();
        Response updatedBookingResponse = putBookingById(id, token);
        if (isStatusOk(updatedBookingResponse)) {
            printResponse(updatedBookingResponse, "\nUpdate booking by ID response:");
            return getResponseJsonElements(updatedBookingResponse, fields);
        }
        return null;
    }

    public int deleteBookingById(int id) {
        Response deletedBookingResponse = deleteBookingById(id, token);
        printResponse(deletedBookingResponse, "\nDeleted booking by ID response:");
        return deletedBookingResponse.getStatusCode();
    }

}
