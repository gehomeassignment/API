package com.codecool.ge.apis;


import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.json.JSONObject;

import static io.restassured.RestAssured.given;

public class AuthApi {

    private final String authEndpoint = "https://restful-booker.herokuapp.com/auth";

    private JSONObject createAuthLoad() {
        JSONObject authLoad = new JSONObject();
        authLoad.put("username", "admin");
        authLoad.put("password", "password123");
        return authLoad;
    }

    private Response postAuth() {
        String load = createAuthLoad().toString();
        Response tokenResponse =
                given()
                        .contentType(ContentType.JSON)
                        .body(load)
                        .post(authEndpoint);
        return tokenResponse;
    }

    public String getToken() {
        String token = postAuth().jsonPath().getString("token");
        return token;
    }
}
