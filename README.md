### Create RESTful API test for ‘restful-booker’

The project was created based on below instructions:

Use the following API documentation - https://restful-booker.herokuapp.com/apidoc/index.html  \
Do 3 actions with help of this API (use JSON format for requests/response):\
    - Create new booking entry with your name – verify booking created with provided details in request and print the response\
    - Get newly created booking entry – verify it’s possible to retrieve the booking and print the response\
    - Update your booking with any information you would like to - verify booking was updated with provided data and print the response\
    - Delete your booking - verify booking was deleted and print the response
